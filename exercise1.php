<?php
function readFileText($fileName)
{
    $openFile = @fopen($fileName, 'r');
    //kiểm tra xem có mở được file không
    if (!$openFile)
    {
        //nếu không mở được thì dừng chương trình
        die('Open file failed');
    }
    //nếu mở được thì tiến hành đọc file
    $content = fread($openFile, filesize($fileName));
    fclose($openFile);
    return $content;
}

function checkValidString($content)
{
    //kiểm tra câu có hợp lệ hay không
    if ((strrpos($content, 'book') !== false && strrpos($content, 'restaurant') === false) || 
    (strrpos($content, 'book') === false && strrpos($content, 'restaurant') !== false)) {
        return true;
    }
    return false;
}

//function hiển thị kết quả
function printResult($fileName)
{
    $contentFile = readFileText($fileName);
    $check_file = checkValidString($contentFile);
    
    if ($check_file) {
        $countSentence = substr_count($contentFile, '.');
        echo 'file '.$fileName.' hop le va co tong so cau la '.$countSentence.' cau<br>'; 
    } else {
        echo 'file '.$fileName.' khong hop le<br>';
    }
}

printResult('file1.txt');
printResult('file2.txt');
?>