<?php
class ExerciseString
{   
    public $check1, $check2;

    function readFile($fileName)
    {
        $openFile = @fopen($fileName, 'r');
        //kiểm tra xem có mở được file không
        if (!$openFile)
        {
            //nếu không mở được thì dừng chương trình
            die('Open file failed');
        }
        //nếu mở được thì tiến hành đọc file
        $content = fread($openFile, filesize($fileName));
        fclose($openFile);
        return $content;
    }

    function checkValidString($content)
    {
        //kiểm tra câu có hợp lệ hay không
        if ((strrpos($content, 'book') !== false && strrpos($content, 'restaurant') === false) || 
        (strrpos($content, 'book') === false && strrpos($content, 'restaurant') !== false)){
            return true;
        }
            return false;
    }

    function writeFile($fileName,$sentence)
    {
        $openFile = @fopen($fileName,'w');
        // kiểm tra xem có đọc được file không
        if (!$openFile) {
            //nếu không mở được thì dừng chương trình
            die('Open file failed');
        }
        //hàm viết vào file
        $content = fwrite($openFile,$sentence);
        fclose($openFile);
    }

    function writeSentece($check, $chuoi)
    {
        return ($check) ? 'chuoi '.$chuoi.' la chuoi hop le'."\n": 'chuoi '.$chuoi.' la chuoi khong hop le'."\n"; 
    }
}

$object1 = new ExerciseString();

$readFile1 = $object1->readFile('file1.txt');
$object1->check1 = $object1->checkValidString($readFile1); 
$str = $object1->writeSentece($object1->check1, 'check1');

$readFile2 = $object1->readFile('file2.txt');
$object1->check2 = $object1->checkValidString($readFile2);
$str .= $object1->writeSentece($object1->check2, 'check2');
$count= substr_count($readFile2, ".");
$str .= "chuoi co ".$count." cau";

$object1->writeFile('result_file.txt', $str);

?>

