<?php
trait Active{
    function defindYourSelf()
    {
        return get_class($this);
    }
}
abstract class Country
{
    protected $slogan;
    abstract function sayHello();
}

interface Boss
{
    function checkValidSlogan();
}

class EnglandCountry extends Country implements Boss
{
    use Active;
    public function sayHello()
    {
        echo 'Helloaa';
    }

    public function setSlogan($content)
    {
        $this->slogan = $content;
    }

    public function checkValidSlogan(){
        return (strripos($this->slogan,'england') !== false || strripos($this->slogan,'english')) !== false ? true : false;
    }
}

class vietnamCountry extends Country implements Boss
{
    use Active;
    public function sayHello()
    {
        echo 'Xin chao';
    }

    public function setSlogan($content)
    {
        $this->slogan = $content;
    }

    public function checkValidSlogan(){
        return ((strripos($this->slogan,'vietnam') !== false && strripos($this->slogan,'hust')) !== false) ? true : false;
    }
}

$englandCountry = new EnglandCountry();
$vietnamCountry = new VietnamCountry();

$englandCountry->setSlogan('England is a country that is part of the United Kingdom. It shares land borders with Wales to 
the west and Scotland to the north. The Irish Sea lies west of England and the Celtic Sea to the southwest.');

$vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. With an estimated 94.6 million 
inhabitants as of 2016, it is the 15th most populous country in the world.');

$englandCountry->sayHello(); // Hello
echo "<br>";
$vietnamCountry->sayHello(); // Xin chao
echo "<br>";

var_dump($englandCountry->checkValidSlogan()); // true
echo "<br>";
var_dump($vietnamCountry->checkValidSlogan()); // false

echo 'I am ' . $englandCountry->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $vietnamCountry->defindYourSelf(); 

